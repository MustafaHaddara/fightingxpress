


#ifndef MMFighter_h
#define MMFighter_h


#include "TSWorld.h"

#include "MMMultiplayer.h"
#include "MMCharacter.h"
#include "MMGamePlayer.h"






namespace MMGame
{
	using namespace Tombstone;
    
    ;;class GamePlayer;
    

       enum : ModelType
        {
                kModelSoldier                   = 'sold'
        };
    
    enum{
        kControllerSoldier
    };




	enum : AnimatorType
	{
		kAnimatorSpineTwist			= 'spin',
		kAnimatorScale				= 'scal',
		kAnimatorWeaponIK			= 'wpik'
	};


	enum : InteractionType
	{
		kInteractionTake			= 'TAKE'
	};



	enum
	{
		kFighterIconChat,
		kFighterIconCount
	};


	enum
	{
		kFighterDead				= 1 << 0,
		kFighterFiringPrimary		= 1 << 1,
		kFighterFiringSecondary		= 1 << 2,
		kFighterTargetDistance		= 1 << 3,

		kFighterFiring				= kFighterFiringPrimary | kFighterFiringSecondary
	};


	enum
	{
		kFighterMotionNone,
		kFighterMotionStop,
		kFighterMotionStand,
		kFighterMotionForward,
		kFighterMotionBackward,
		kFighterMotionTurnLeft,
		kFighterMotionTurnRight,
		kFighterMotionJump,
		kFighterMotionDeath
	};

    /**
	* 	Handles rotation of the players spine
	*/
    class SpineTwistAnimator : public Animator
    {
    private:
        
        int32			superNodeTransformIndex[2];
        Quaternion		spineRotation;
        
        SpineTwistAnimator();
        
    public:
        SpineTwistAnimator(Model *model, Node *node);
        ~SpineTwistAnimator();
        
		/**
		*	Sets the spine rotation to the Quaternion q
		*/
        void SetSpineRotation(const Quaternion& q)
        {
            spineRotation = q;
        }
        
		/**
		* Sets up the class to be used
		*/
        void PreprocessAnimator(void) override;
		/**
		* Configures the animator for later use
		*/
        void ConfigureAnimator(void) override;
		/**
		* Moves the spine of the character
		*/
        void MoveAnimator(void) override;
    };

	class FighterController;

	/**
	* Handles interactions between the player and another node
	*/
	class FighterInteractor final : public Interactor
	{
		private:

			FighterController	*fighterController;

		public:

			FighterInteractor(FighterController *controller);
			~FighterInteractor();
			/**
			* The function to handle interactions between the player and another node
			*/
			void HandleInteractionEvent(InteractionEventType type, Node *node, const InteractionProperty *property, const Point3D *position = nullptr) override;
	};

	/**
	* Controller for the players character. Handles all related messages, updates, movement and forces applied to the character.
	*/
	class FighterController : public GameCharacterController
	{
		private:

			unsigned_int32			fighterFlags;
			Link<Player>			fighterPlayer;
 
			float					primaryAzimuth; 
			float					modelAzimuth;
 
			float					lookAzimuth; 
			float					lookAltitude; 
			float					deltaLookAzimuth;
			float					deltaLookAltitude; 
			float					lookInterpolateParam; 
 
			Point3D					previousCenterOfMass;

			unsigned_int32			movementFlags; 
			int32					fighterMotion;
			bool					motionComplete;

			Vector3D				firingDirection;
			float					targetDistance;

			int32					damageTime;
			unsigned_int32			weaponSwitchTime;

			Model					*weaponModel;
			const Marker			*weaponFireMarker;
			const Marker			*weaponMountMarker;

			int32					iconIndex;
			QuadEffect				*iconEffect;

			Node					*mountNode;
			Light					*flashlight;

			MergeAnimator			*rootAnimator;
			MergeAnimator			*mergeAnimator;
			BlendAnimator			*blendAnimator;
			FrameAnimator			*frameAnimator[2];
        
            SpineTwistAnimator			*spineTwistAnimator;
        
            int32                   currentControllerHealth;
			int32                   deathTime;
			
            int                     currentGlobalRotationDirection;
            bool                    rotationFlag;
            int32                   rotationCounter;
            Transform4D             currentMapRotation;
        
            // MVM
            int playerkey;


			FighterInteractor								fighterInteractor;
			Observer<FighterController, WorldObservable>	worldUpdateObserver;
			FrameAnimatorObserver<FighterController>		frameAnimatorObserver;

			void SetOrientation(float azm, float alt)
			{
				lookAzimuth = azm;
				lookAltitude = alt;
				lookInterpolateParam = 0.0F;
			}
        

			void SetMountNodeTransform(void);

			void HandleWorldUpdate(WorldObservable *observable);
			void HandleAnimationEvent(FrameAnimator *animator, CueType cueType);

			static void HandleMotionCompletion(Interpolator *interpolator, void *cookie);


		protected:


			float GetModelAzimuth(void) const
			{
				return (modelAzimuth);
			}

			float GetInterpolatedLookAzimuth(void) const
			{
				return (lookAzimuth + deltaLookAzimuth * lookInterpolateParam);
			}

			float GetInterpolatedLookAltitude(void) const
			{
				return (lookAltitude + deltaLookAltitude * lookInterpolateParam);
			}

			MergeAnimator *GetMergeAnimator(void) const
			{
				return (mergeAnimator);
			}

			BlendAnimator *GetBlendAnimator(void) const
			{
				return (blendAnimator);
			}

			FrameAnimator *GetFrameAnimator(int32 index) const
			{
				return (frameAnimator[index]);
			}

			void SetFrameAnimatorObserver(FrameAnimator::ObserverType *observer)
			{
				frameAnimator[0]->SetObserver(observer);
				frameAnimator[1]->SetObserver(observer);
			}

		public:
			int32                   endTime;

            int32 GetCurrentControllerHealth(void)
            {
                return (currentControllerHealth);
            }
        
            int GetCurrentGlobalRotationDirection(void)
            {
                return (currentGlobalRotationDirection);
            }
        
            bool GetCurrentRotationFlag(void)
            {
                return (rotationFlag);
            }
        
            bool GetCurrentRotationCounter(void)
            {
                return (rotationCounter);
            }
			int32                   currentPlayerScore;

            void SetCurrentControllerHealth(int32 newHealth);
            void SetCurrentGlobalRotationDirection(int newGlobalRotationDirection);
            void SetCurrentRotationFlag(bool newRotationFlag);
            void SetCurrentRotationCounter(bool newRotationCounter);
			
			/**
			* Gets the map rotation for later use.
			*/
            void InitializeCurrentMapRotation();
            void SetCurrentMapRotation(Transform4D serverMapRotation, int serverGlobalRotationDirection);
        
			/**
			* Handles shooting the gun. Raycasts from the point specified by firingAzimuth and firingAltitude
			*/
            void fireLaser(float firingAzimuth, float firingAltitude);

			FighterController(ControllerType type);

			enum
			{
				kFighterMessageEngageInteraction = kRigidBodyMessageBaseCount,
				kFighterMessageDisengageInteraction,
				kFighterMessageBeginMovement,
				kFighterMessageEndMovement,
				kFighterMessageChangeMovement,
				kFighterMessageBeginShield,
				kFighterMessageEndShield,
				kFighterMessageBeginIcon,
				kFighterMessageEndIcon,
				kFighterMessageTeleport,
				kFighterMessageLaunch,
				kFighterMessageLand,
				kFighterMessageUpdate,
				kFighterMessageWeapon,
				kFighterMessageEmptyAmmo,
				kFighterMessageDamage,
				kFighterMessageDeath
			};

			~FighterController();

			unsigned_int32 GetFighterFlags(void) const
			{
				return (fighterFlags);
			}

			void SetFighterFlags(unsigned_int32 flags)
			{
				fighterFlags = flags;
		
            }

			Point3D GetPosition(void)
			{
				Node* node= GetTargetNode();
				return(node->GetWorldPosition());
			}

			void SetFighterPlayer(Player *player)
			{
				fighterPlayer = player;
			}

			float GetPrimaryAzimuth(void) const
			{
				return (primaryAzimuth);
			}

			void SetPrimaryAzimuth(float azimuth)
			{
				primaryAzimuth = azimuth;
			}

			float GetLookAzimuth(void) const
			{
				return (lookAzimuth);
			}

			void SetLookAzimuth(float azimuth)
			{
				lookAzimuth = azimuth;
			}

			float GetLookAltitude(void) const
			{
				return (lookAltitude);
			}

			void SetLookAltitude(float altitude)
			{
				lookAltitude = altitude;
			}

			unsigned_int32 GetMovementFlags(void) const
			{
				return (movementFlags);
			}

			void SetMovementFlags(unsigned_int32 flags)
			{
				movementFlags = flags;
			}

			int32 GetFighterMotion(void) const
			{
				return (fighterMotion);
			}

			const Vector3D& GetFiringDirection(void) const
			{
				return (firingDirection);
			}

			float GetTargetDistance(void) const
			{
				return (targetDistance);
			}

			void SetTargetDistance(float distance)
			{
				targetDistance = distance;
			}

			Model *GetWeaponModel(void) const
			{
				return (weaponModel);
			}

			const Marker *GetWeaponFireMarker(void) const
			{
				return (weaponFireMarker);
			}


			Light *GetFlashlight(void) const
			{
				return (flashlight);
			}

			FighterInteractor *GetFighterInteractor(void)
			{
				return (&fighterInteractor);
			}

			const FighterInteractor *GetFighterInteractor(void) const
			{
				return (&fighterInteractor);
			}
			/**
			* Compressess all of data.
			*/
			void Pack(Packer& data, unsigned_int32 packFlags) const override;
			/**
			* Decompressess all of data.
			*/
			void Unpack(Unpacker& data, unsigned_int32 unpackFlags) override;
			/**
			* Compressess only the first piece of data.
			*/
			void UnpackChunk(const ChunkHeader *chunkHeader, Unpacker& data, unsigned_int32 unpackFlags);

			/**
			* Sets up the player to be used. Initalizes the model and animator.
			*/
			void PreprocessController(void) override;
			/**
			* Handles updating the player each frame.
			*/
			void MoveController(void) override;

			/**
			* Creates a message based on the type specified.
			*/
			ControllerMessage *CreateMessage(ControllerMessageType type) const override;
			/**
			* Executes an action specific to the type of message.
			*/
			void ReceiveMessage(const ControllerMessage *message) override;
			/**
			* Creates the player and sends them their player and the world position.
			*/
			void SendInitialStateMessages(Player *player) const override;
			/**
			* Sends the positions of all players to all players, syncing their postions within their worlds
			*/
			void SendSnapshot(void) override;

			void EnterWorld(World *world, const Point3D& worldPosition) override;

			/**
			* Handles sending a message in the event a player receives damage. Also sends the updated score and kills the damaged player if necessary.
			*/
			CharacterStatus Damage(int damage, unsigned_int32 flags, GameCharacterController *attacker, const Point3D *position = nullptr, const Vector3D *impulse = nullptr) override;
			/**
			* Kills the player and updates the score of the player that got the kill. Also locks the controls and begins the respawn timer for the dead player.
			*/
			void Kill(GameCharacterController *attacker, const Point3D *position = nullptr, const Vector3D *impulse = nullptr) override;

			/**
			* Updates the characters orientation.
			*/
			void UpdateOrientation(float azm, float alt);
			/**
			* Sends a message to all players that this character is beginning to move.
			*/
			void BeginMovement(unsigned_int32 flag, float azm, float alt);
			/**
			* Sends a message to all players that this character is ceasing movement.
			*/
			void EndMovement(unsigned_int32 flag, float azm, float alt);
			/**
			* Sends a message to all players To update the movement of this character.
			*/
			void ChangeMovement(unsigned_int32 flags, float azm, float alt);

			/**
			* Sets the characters orientation and sets flags indicating firing has began.
			*/
			void BeginFiring(bool primary, float azm, float alt);
			/**
			* Sets the characters orientation and sets flags indicating firing has ceased.
			*/
			void EndFiring(float azm, float alt);
			/**
			* The SetPerspectiveExclusionMask function sets the perspective exclusion mask that determines from what camera perspectives the node is enabled to the value specified by the mask parameter.
			*/
			void SetPerspectiveExclusionMask(unsigned_int32 mask) const;
			/**
			* Sets the characters current action to the type specified in motion
			*/
			virtual void SetFighterMotion(int32 motion);
			/**
			* Plays an animation for the character.
			*/
            void AnimateFighter(void);
        
        void SetPlayerKey(int key){
            playerkey=key;
        }
        /**
		* Enables the controller if it is disabled.
		*/
		void WakeController(void) override;
    };
	/**
	* Encapsulates creating a message related to creating a Fighter
	*/
	class CreateFighterMessage : public CreateModelMessage
	{
		private:

			float				initialAzimuth;
			float				initialAltitude;

			unsigned_int32		movementFlags;

			int32				weaponIndex;
			int32				weaponControllerIndex;

			int32				playerKey;

        public:

			CreateFighterMessage(ModelMessageType type);
			~CreateFighterMessage();

		public:

			CreateFighterMessage(ModelMessageType type, int32 fighterIndex, const Point3D& position, float azm, float alt, unsigned_int32 movement, int32 weapon, int32 weaponController, int32 key);

			float GetInitialAzimuth(void) const
			{
				return (initialAzimuth);
			}

			float GetInitialAltitude(void) const
			{
				return (initialAltitude);
			}

			unsigned_int32 GetMovementFlags(void) const
			{
				return (movementFlags);
			}

			int32 GetWeaponIndex(void) const
			{
				return (weaponIndex);
			}

			int32 GetWeaponControllerIndex(void) const
			{
				return (weaponControllerIndex);
			}

			int32 GetPlayerKey(void) const
			{
				return (playerKey);
			}
			/**
			* Compressess data.
			*/
			void CompressMessage(Compressor& data) const override;
			/**
			* Decompressess data.
			*/
			bool DecompressMessage(Decompressor& data) override;
	};



	/**
	* Encapsulates creating a message for Fighter movement.
	*/
	class FighterMovementMessage : public CharacterStateMessage
	{
		friend class FighterController;

		private:

			float				movementAzimuth;
			float				movementAltitude;
			unsigned_int32		movementFlag;

			FighterMovementMessage(ControllerMessageType type, int32 controllerIndex);

		public:

			FighterMovementMessage(ControllerMessageType type, int32 controllerIndex, const Point3D& position, const Vector3D& velocity, float azimuth, float altitude, unsigned_int32 flag);
			~FighterMovementMessage();

			float GetMovementAzimuth(void) const
			{
				return (movementAzimuth);
			}

			float GetMovementAltitude(void) const
			{
				return (movementAltitude);
			}

			int32 GetMovementFlag(void) const
			{
				return (movementFlag);
			}

			/**
			* Compressess data.
			*/
			void CompressMessage(Compressor& data) const override;
			/**
			* Decompressess data.
			*/
			bool DecompressMessage(Decompressor& data) override;
	};

	/**
	* Encapsulates updating the position and orientation of the Fighter
	*/
	class FighterUpdateMessage : public ControllerMessage
	{
		friend class FighterController;

		private:

			float		updateAzimuth;
			float		updateAltitude;

			FighterUpdateMessage(int32 controllerIndex);

		public:

			FighterUpdateMessage(int32 controllerIndex, float azimuth, float altitude);
			~FighterUpdateMessage();

			float GetUpdateAzimuth(void) const
			{
				return (updateAzimuth);
			}

			float GetUpdateAltitude(void) const
			{
				return (updateAltitude);
			}

			/**
			* Compressess data.
			*/
			void CompressMessage(Compressor& data) const override;
			/**
			* Decompressess data.
			*/
			bool DecompressMessage(Decompressor& data) override;
	};

	class ScaleAnimator : public Animator
	{
		private:

			float		scale;

			ScaleAnimator();

		public:

			ScaleAnimator(Model *model, Node *node = nullptr);
			~ScaleAnimator();

			void SetScale(float s)
			{
				scale = s;
			}

			void ConfigureAnimator(void) override;
			void MoveAnimator(void) override;
	};
}
#endif